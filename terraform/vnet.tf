## Azure Vnet resource creation
data "azurerm_resource_group" "rg"{
  name = var.resource_group_name
}

resource "azurerm_virtual_network" "vnet" {
  name                = "${var.business_divsion}-${var.environment}-${var.name}"
  # name                = var.name
  address_space       = ["${var.address_space}"]
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "local_file" "file" {
  content = var.text
  filename = "${path.module}/${var.filename}.txt"
  directory_permission = "0755"
  file_permission = "0644"
}
